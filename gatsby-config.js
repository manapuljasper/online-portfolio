module.exports = {
  pathPrefix: `/online-portfolio`,
  siteMetadata: {
    title: 'Ian Jasper Manapul',
    author: 'Hunter Chang',
    description: "Ian Jasper Manapul's Online Portfolio done with Gatsby",
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'gatsby-starter-default',
        short_name: 'starter',
        start_url: '/',
        background_color: '#663399',
        theme_color: '#663399',
        display: 'minimal-ui',
        icon: 'src/images/hiclipart.com.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
  ],
}
